import { IController } from '@/services/api'

import { GeoController } from './geo'

export const controllers: [IController] = [
  new GeoController(),
]